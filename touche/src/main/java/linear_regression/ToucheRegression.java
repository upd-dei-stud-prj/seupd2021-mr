package linear_regression;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.LinearRegression;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.supervised.instance.StratifiedRemoveFolds;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Regression for argument logical validity
 * Predict if a given argument is well written
 */
public class ToucheRegression{

    private final LinearRegression regressionModel;
    private Instances trainSet;
    private Instances testSet;

    public ToucheRegression() {
        regressionModel = new LinearRegression();
    }

    /**
     * Transform a csv file into weka instances.
     * @param path location of csv file
     * @return Instances version of csv file
     */
    public Instances fromCsvToInstances(String path) throws IOException {
        CSVLoader loaderCleanData = new CSVLoader();
        loaderCleanData.setFieldSeparator(",");
        loaderCleanData.setSource(new File(path));
        return loaderCleanData.getDataSet();
    }

    /**
     * Function to load the data of my regression
     * and automatically split it to training and set test
     *
     * @param locationPathData location of encoded clean data
     * @throws IOException if not able to manage the file
     */
    public void loadTestTrainingData(String locationPathData) throws Exception {
        Instances cleanData = fromCsvToInstances(locationPathData);
        cleanData.setClassIndex(cleanData.numAttributes()-1);
        // use StratifiedRemoveFolds to randomly split the data
        StratifiedRemoveFolds filter = new StratifiedRemoveFolds();

        // set options for creating the subset of data
        String[] options = new String[6];

        options[0] = "-N";                 // indicate we want to set the number of folds
        options[1] = Integer.toString(5);  // split the data into five random folds
        options[2] = "-F";                 // indicate we want to select a specific fold
        options[3] = Integer.toString(1);  // select the first fold
        options[4] = "-S";                 // indicate we want to set the random seed
        options[5] = Integer.toString(1);  // set the random seed to 1

        filter.setOptions(options);        // set the filter options
        filter.setInputFormat(cleanData);       // prepare the filter for the data format
        filter.setInvertSelection(false);  // do not invert the selection

        // apply filter for test data here
        testSet = Filter.useFilter(cleanData, filter);

        //  prepare and apply filter for training data here
        filter.setInvertSelection(true);     // invert the selection to get other data
        trainSet = Filter.useFilter(cleanData, filter);
    }

    /**
     * Function to load only the training data of my regression
     *
     * @param locationPathData location of encoded clean data
     * @throws IOException if not able to manage the file
     */
    public void loadTrainingData(String locationPathData) throws Exception {
        CSVLoader loaderCleanData = new CSVLoader();
        loaderCleanData.setFieldSeparator(",");
        loaderCleanData.setSource(new File(locationPathData));
        trainSet = loaderCleanData.getDataSet();
        trainSet.setClassIndex(trainSet.numAttributes()-1);
    }

    /**
     * Build the regression model upon {@code data}
     */
    public void build() throws Exception {
        regressionModel.buildClassifier(trainSet);
    }

    /**
     * Classify given samples
     * @return array of predictions
     */
    public ArrayList<Double> classifyInstances(Instances samples) throws Exception {
        ArrayList<Double> predictions = new ArrayList<>();
        for(Instance instance : samples){
            predictions.add(regressionModel.classifyInstance(instance));
        }
        return predictions;
    }


    /**
     * Classify test set
     * @return array of predictions
     */
    public ArrayList<Double> classifyTestSet() throws Exception {
        ArrayList<Double> predictions = new ArrayList<>();
        for(Instance instance : testSet){
            predictions.add(regressionModel.classifyInstance(instance));
        }
        return predictions;
    }

    /**
     * Classify test set
     * @return array of predictions
     */
    public Double classify(Instance data) throws Exception {
        return regressionModel.classifyInstance(data);
    }

    /**
     *
     * Function to evaluate our system
     * @return root mean squared error not normalized (can go over 1)
     */
    public Double evaluation() throws Exception {
        Evaluation evaluation = new Evaluation(testSet);
        evaluation.crossValidateModel(regressionModel, testSet, 3, new Random((1)));
        return evaluation.rootMeanSquaredError();
    }
}
