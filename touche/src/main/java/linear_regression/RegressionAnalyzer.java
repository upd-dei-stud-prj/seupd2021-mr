package linear_regression;

import exec.Utils;
import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.miscellaneous.LengthFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

import java.io.Reader;

/**
 * Custom analyzer used to process all the documents retrieved from the ArgumentDataset containing the dataset
 * used for the machine learning part
 */
public class RegressionAnalyzer extends Analyzer {

    /**
     * Creates a new instance of the analyzer.
     */
    public RegressionAnalyzer() {
        super();
    }

    @Override
    protected Analyzer.TokenStreamComponents createComponents(String fieldName) {

        //final Tokenizer source = new WhitespaceTokenizer();
        //final Tokenizer source = new LetterTokenizer();
        final Tokenizer source = new StandardTokenizer();

        TokenStream tokens = new LowerCaseFilter(source);

        tokens = new LengthFilter(tokens, 4, 10);

        //tokens = new EnglishPossessiveFilter(tokens);

        tokens = new StopFilter(tokens, Utils.loadStopList("stoplist.txt"));

        //tokens = new EnglishMinimalStemFilter(tokens);
        //tokens = new PorterStemFilter(tokens);
        //tokens = new KStemFilter(tokens);
        //tokens = new LovinsStemFilter(tokens);


        return new Analyzer.TokenStreamComponents(source, tokens);
    }

    @Override
    protected Reader initReader(String fieldName, Reader reader) {
        // return new HTMLStripCharFilter(reader);

        return super.initReader(fieldName, reader);
    }

    @Override
    protected TokenStream normalize(String fieldName, TokenStream in) {
        return new LowerCaseFilter(in);
    }
}
