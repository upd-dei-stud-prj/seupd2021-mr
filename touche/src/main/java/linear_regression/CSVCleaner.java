package linear_regression;

import weka.core.Instances;
import weka.core.converters.CSVLoader;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Helper to clean a touche csv file.
 * The table structure has 12 column features.
 */
public class CSVCleaner {
    /**
     * Method used to remove quotation marks inside documents of the dataset
     * and to add quotation marks at the start and at the end if they are not present
     *
     * @param fileToRead  ArgumentDataset.csv
     * @param fileToWrite ArgumentDatasetModified.csv
     */
    public static void removeQuotationMarks(String fileToRead, String fileToWrite) throws IOException
    {
        BufferedReader csvReader = Files.newBufferedReader(Paths.get(fileToRead));
        FileWriter csvWriter = new FileWriter(Paths.get(fileToWrite).toFile());
        String row = csvReader.readLine();

        // Append first row (the header) on new file
        csvWriter.append(row);
        csvWriter.append("\n");

        while ((row = csvReader.readLine()) != null) {

            // Split on each quotation mark
            String[] data = row.split("\"");

            /*
            If data length is 1, split on each comma and add
            quotation marks at the start and at the end of the document
            */
            if (data.length == 1) {
                String[] columns = row.split(",");
                for (int i = 0; i < columns.length; i++) {

                    // If i == 3 the column is the document
                    if (i == 3) {
                        csvWriter.append("\"");
                        csvWriter.append(columns[i]);
                        csvWriter.append("\"");
                        csvWriter.append(",");
                    } else if (i == columns.length - 1) {
                        csvWriter.append(columns[i]);
                    } else {
                        csvWriter.append(columns[i]);
                        csvWriter.append(",");
                    }
                }
                csvWriter.append("\n");
            }
            /*
            If data length is greater than 1, remove quotation marks
            inside the document
            */
            else if (data.length > 1) {
                csvWriter.append(data[0]);
                csvWriter.append("\"");
                for (int i = 1; i < data.length - 1; i++) {
                    csvWriter.append(data[i]);
                }
                csvWriter.append("\"");
                csvWriter.append(data[data.length - 1]);
                csvWriter.append("\n");
            }
        }

        csvWriter.flush();
        csvWriter.close();
        csvReader.close();
    }

    /**
     * Method to make the dataset machine learning palatable.
     * Above the 12 features, extract only "Premise" and "Combined quality" and encode it.
     * @param path where the dataset.csv is
     * @param finalPath to locate the cleaned/encoded file
     * @param maxWordCount max number of most frequent words to consider
     * @throws IOException if not able to manage the file
     */
    public void cleanDataset(String path, String finalPath, int maxWordCount) throws IOException {
        CSVLoader loader = new CSVLoader();
        loader.setFieldSeparator(",");
        loader.setSource(new File(path));
        Instances rawData = loader.getDataSet();
        //clean the rawData (there is more clever ways to do it, but for our purpose it's enough)
        rawData.deleteAttributeAt(0); //eliminate Topic ID
        rawData.deleteAttributeAt(0); //eliminate Argument ID
        rawData.deleteAttributeAt(0); //eliminate Discussion ID
        rawData.deleteAttributeAt(1); //eliminate Relevance
        rawData.deleteAttributeAt(1); //eliminate is Argument?
        rawData.deleteAttributeAt(1); //eliminate Rhetorical Quality
        rawData.deleteAttributeAt(1); //eliminate Logical Quality
        rawData.deleteAttributeAt(1); //eliminate Dialectical Quality
        rawData.deleteAttributeAt(1); //eliminate Text Length
        rawData.deleteAttributeAt(1); //eliminate Stance
        //Now we have only two attributes, the argument and the combined quality score of that argument

        //Let's encode our data in order to use it for machine learning task
        Encoder encoder = new Encoder();
        encoder.extractFeature(rawData, finalPath, maxWordCount);
    }
}
