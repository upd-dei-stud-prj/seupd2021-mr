package linear_regression;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.*;

/**
 * Helper class to load stop lists and to filter {@link TokenStream}s
 */
public class AnalyzerUtil {

    private static final ClassLoader CL = AnalyzerUtil.class.getClassLoader();

    /**
     * Clean a string using the provided analyzer.
     *
     * @param a the analyzer to use.
     * @param t the text to process.
     *
     * @throws IOException if something goes wrong while processing the text.
     */
    public static String cleanTokenStream(final Analyzer a, final String t) throws IOException {

        StringBuilder cleanedString = new StringBuilder();

        // Create a new TokenStream for a dummy field
        final TokenStream stream = a.tokenStream("field", new StringReader(t));


        // The term represented by the token
        final CharTermAttribute tokenTerm = stream.addAttribute(CharTermAttribute.class);

        try {
            // Reset the stream before starting
            stream.reset();

            // build the final string
            while (stream.incrementToken()) {
                cleanedString.append(tokenTerm.toString()).append(" ");
            }

            // Perform any end-of-stream operations
            stream.end();
        } finally {

            // Close the stream and release all the resources
            stream.close();
        }

        return cleanedString.toString();
    }
}