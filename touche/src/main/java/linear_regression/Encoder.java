package linear_regression;

import weka.core.Instances;

import java.io.IOException;
import java.util.*;

import static java.util.stream.Collectors.toMap;

/**
 * Class to encode text feature from an {@code Instances} object.
 * Write the encoded version in a csv file, ready to be used.
 */
public class Encoder {

    public Encoder(){}

    /**
     * Method to clean the {@code data} using stemming and stop list
     * @param data instances to clean
     * @param path to locate the cleaned data
     * @param maxWordCount max number of most frequent words to consider
     * @return labels corresponding to each text
     * @throws IOException
     */
    public void extractFeature(Instances data, String path, int maxWordCount) throws IOException {
        ArrayList<String> cleanedDataset = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<>();
        for(int i = 0; i<data.size(); i++) {
            String currentInstance = data.instance(i).toString();
            int index = currentInstance.lastIndexOf(",");
            String current = currentInstance.substring(0, index);
            labels.add(currentInstance.substring(index+1));
            cleanedDataset.add(AnalyzerUtil.cleanTokenStream(new RegressionAnalyzer(), current));
        }
        featureExtractor(cleanedDataset,  labels, maxWordCount, path);
    }

    /**
     * Function to extract a features vector from a set of text samples (strings).
     * @param dataset ArrayList of text samples
     * @param labels only in the case of a training set construction
     * @param maxWordCount max number of words to consider
     */
    private void featureExtractor(ArrayList<String> dataset, ArrayList<String> labels, int maxWordCount, String path){
        Map<String, Integer> sortedWordCountFrequency = wordFrequency(dataset);

        //Fill a vector with the first most frequent maxWordCount words
        ArrayList<String> mostFrequentWords = new ArrayList<>();
        int maxWordIndex = 0;
        for(Map.Entry<String, Integer> word : sortedWordCountFrequency.entrySet()){
            mostFrequentWords.add(word.getKey());
            if(maxWordIndex == maxWordCount-1){
                mostFrequentWords.add("Combined Quality"); //add the last attributes
                break; //and we are finished
            }
            else maxWordIndex++;
        }

        //Encode the dataset
        ArrayList<Double[]> featureSet = multiHotEncoding(dataset, sortedWordCountFrequency, labels, maxWordCount);
        new CSVWriter().csvFileWriter(featureSet, mostFrequentWords, path);
    }

    /**
     * Function to extract a features vector from a set of text samples (strings).
     * @param dataset ArrayList of text samples
     * @param maxWordCount max number of words to consider
     */
    public void featureExtractor(ArrayList<String> dataset, int maxWordCount, String path){
        Map<String, Integer> sortedWordCountFrequency = wordFrequency(dataset);

        //Fill a vector with the first most frequent maxWordCount words
        ArrayList<String> mostFrequentWords = new ArrayList<>();
        int maxWordIndex = 0;
        for(Map.Entry<String, Integer> word : sortedWordCountFrequency.entrySet()){
            mostFrequentWords.add(word.getKey());
            if(maxWordIndex == maxWordCount-1){
                break; //and we are finished
            }
            else maxWordIndex++;
        }

        //Encode the dataset
        ArrayList<Double[]> featureSet = multiHotEncoding(dataset, sortedWordCountFrequency, maxWordCount);
        new CSVWriter().csvFileWriter(featureSet, mostFrequentWords, path);
    }

    /**
     * Function to calculate the frequency of each word in the dataset
     * @param dataset of strings
     * @return a map which entry is made of (word, frequency of that word in the dataset) in
     * descending order by value (from the most frequent to the last frequent)
     */
    public Map<String, Integer> wordFrequency(ArrayList<String> dataset){
        Map<String, Integer> wordFrequencyCount = new HashMap<>();

        //Need to count the global frequencies of the words
        for(int i = 0; i< dataset.size(); i++){
            //For each samples, consider word by word and memorize the count of them
            String[] tokens = dataset.get(i).split(" ");
            for(int tokenIndex = 0; tokenIndex < tokens.length; tokenIndex++)
                if(wordFrequencyCount.containsKey(tokens[tokenIndex])){
                    //Update the count of that word
                    int count = wordFrequencyCount.get(tokens[tokenIndex]);
                    count++;
                    wordFrequencyCount.put(tokens[tokenIndex],count);
                }
                else{
                    //New word, count is 1
                    int count = 1;
                    wordFrequencyCount.put(tokens[tokenIndex], count);
                }
        }
        //Now we have a dictionary filled with (word, count of that word)
        //Sort from the most frequent to less frequent word
        Map<String, Integer> sortedWordCountFrequency = wordFrequencyCount
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                                LinkedHashMap::new));
        return sortedWordCountFrequency;
    }

    /**
     * Function to encode a sample (text and label) in a vector of {0,1} of size maxWordCount.
     * A position in the vector represent a word in sortedWordCountFrequency. (first maxWordCount most frequent words)
     * If my current sample contains that word, its position value becomes 1, 0 otherwise.
     * Note: {0,1} can be substituted with any double values. i.e. idf, relative frequency and so on. Just build a different function
     * Use this to encode training set.
     * @param dataset of my samples
     * @param sortedWordCountFrequency map containing in descending order by value (word, absoluteFrequency)
     * @param labels of my samples
     * @param maxWordCount dimension of my frequency vector
     * @return
     */
    public ArrayList<Double[]> multiHotEncoding(ArrayList<String> dataset, Map<String, Integer> sortedWordCountFrequency, ArrayList<String> labels, int maxWordCount){
        ArrayList<Double[]> featureSet = new ArrayList<>();
        for(int i=0; i<dataset.size(); i++){
            String currentSample = dataset.get(i);//Take a sample ("bla bla bla")
            //Each sample must be represented as a vector. v[0] represent most frequent word in the sample,
            //v[1] second most frequent and so on
            Double[] encoded = new Double[maxWordCount+1]; //+1 to add the labels to predict
            int wordIndex = 0;
            for (Map.Entry<String, Integer> entry : sortedWordCountFrequency.entrySet()) {
                if(currentSample.contains(entry.getKey())){
                    //The word is present in the current sample. Set the flag for this word to 1
                    encoded[wordIndex] = 1.0;
                }
                else{
                    //The word is not present :( Set the flag to 0
                    encoded[wordIndex] = 0.0;
                }
                if(wordIndex < maxWordCount-1) wordIndex++; //To avoid indexOutOfRange
                else{
                    //if I'm here I must fill the last element of my vector with the corresponding label
                    encoded[++wordIndex] = Double.parseDouble(labels.get(i));
                    break; // I filled all my vector
                }
            }
            // Finally our feature is ready, add to the features data set
            featureSet.add(encoded);
        }
        return featureSet;
    }

    /**
     * Function to encode a sample (text only) in a vector of {0,1} of size maxWordCount.
     * A position in the vector represent a word in sortedWordCountFrequency. (first maxWordCount most frequent words)
     * If my current sample contains that word, its position value becomes 1, 0 otherwise.
     * Note: {0,1} can be substituted with any double values. i.e. idf, relative frequency and so on. Just build a different function.
     * Use this to encode sample to classify
     * @param dataset of my samples
     * @param sortedWordCountFrequency map containing in descending order by value (word, absoluteFrequency)
     * @param maxWordCount dimension of my frequency vector
     * @return
     */
    public ArrayList<Double[]> multiHotEncoding(ArrayList<String> dataset, Map<String, Integer> sortedWordCountFrequency, int maxWordCount){
        ArrayList<Double[]> featureSet = new ArrayList<>();
        for(int i=0; i<dataset.size(); i++){
            String currentSample = dataset.get(i);//Take a sample ("bla bla bla")
            //Each sample must be represented as a vector. v[0] represent most frequent word in the sample,
            //v[1] second most frequent and so on
            Double[] encoded = new Double[maxWordCount];
            int wordIndex = 0;
            for (Map.Entry<String, Integer> entry : sortedWordCountFrequency.entrySet()) {
                if(currentSample.contains(entry.getKey())){
                    //The word is present in the current sample. Set the flag for this word to 1
                    encoded[wordIndex] = 1.0;
                }
                else{
                    //The word is not present :( Set the flag to 0
                    encoded[wordIndex] = 0.0;
                }
                if(wordIndex < maxWordCount-1) wordIndex++; //To avoid indexOutOfRange
                else{
                    break; // I filled all my vector
                }
            }
            // Finally our feature is ready, add to the features data set
            featureSet.add(encoded);
        }
        return featureSet;
    }
}
