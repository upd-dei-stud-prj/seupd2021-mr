package linear_regression;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class containing custom function to write a csv file for {@code ToucheRegression}
 */
public class CSVWriter {

    public CSVWriter(){}
    /**
     * Function to write a Csv file given an encoded set of values
     * @param multiHotEncodedSet of values
     * @param mostFrequentWords of first maxWordCount words
     */
    void csvFileWriter(ArrayList<Double[]> multiHotEncodedSet, ArrayList<String> mostFrequentWords, String path){
        File csvFile = new File(path);
        try (PrintWriter pw = new PrintWriter(csvFile)) {
            //write the first row of labels
            for(int i=0; i<mostFrequentWords.size(); i++) {
                if(i<mostFrequentWords.size()-1) pw.write(mostFrequentWords.get(i) + ",");
                else pw.write(mostFrequentWords.get(i));
            }
            pw.write("\n");
            multiHotEncodedSet.stream()
                    .map(this::convertToCSV)
                    .forEach(pw::println);

            pw.flush();
        }
        catch (IOException e) {
            //Handle exception
            e.printStackTrace();
        }
    }


    /**
     * Support function to {@code csvFileWriter}
     * @param data to write
     * @return the string to write in the file
     */
    public String convertToCSV(Double[] data) {
        return Stream.of(data)
                .map(this::escapeSpecialCharacters)
                .collect(Collectors.joining(","));
    }

    /**
     * Eventually skip special character, cast when necessary
     * @param data to transform to string
     * @return the data casted
     */
    public String escapeSpecialCharacters(Double data) {
        return data.toString();
    }
}
