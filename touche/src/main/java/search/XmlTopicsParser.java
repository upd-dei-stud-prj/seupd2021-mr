package search;

import org.apache.lucene.benchmark.quality.QualityQuery;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Topics parser using javax.xml.parsers library and DOM API
 */
public class XmlTopicsParser {

    /**
     * Function used to extract all the queries from a .xml file
     *
     * @param filename path of the files containing the topics
     * @return an array of {@code QualityQuery}. Each element represents a single query in lucene format.
     */
    public QualityQuery[] readQueries(String filename) throws ParserConfigurationException, IOException, SAXException {
        ArrayList<QualityQuery> topicsList = new ArrayList<>();

        // Load the xml file in the main memory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new File(filename));

        // Get the topic list element
        NodeList list = doc.getElementsByTagName("topic");

        // Fore each element inside the topic list extract a query
        for(int i=0; i<list.getLength(); i++){
            Node node = list.item(i);

            if(node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;

                // Extract all the fields of the query
                Node idNode = element.getElementsByTagName("number").item(0);
                Node titleNode = element.getElementsByTagName("title").item(0);
                Node descriptionNode = element.getElementsByTagName("description").item(0);
                Node narrativeNode = element.getElementsByTagName("narrative").item(0);

                String id = idNode == null ? "" : idNode.getTextContent();
                String title = titleNode == null ? "" : titleNode.getTextContent();
                String description = descriptionNode == null ? "" : descriptionNode.getTextContent();
                String narrative = narrativeNode == null ? "" : narrativeNode.getTextContent();

                HashMap<String,String> fields = new HashMap<>();
                fields.put("title", title);
                fields.put("description", description);
                fields.put("narrative", narrative);

                // Add the query in the list
                topicsList.add(new QualityQuery(id, fields));
            }
        }

        // Sort result array by ID and return it
        QualityQuery[] qq = topicsList.toArray(new QualityQuery[0]);
        Arrays.sort(qq);

        return qq;
    }
}
