package search;

import exec.Utils;
import linear_regression.CSVCleaner;
import linear_regression.Encoder;
import linear_regression.ToucheRegression;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.util.BytesRef;
import parse.ParsedDocument;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.benchmark.quality.QualityQuery;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;
import scala.Tuple4;
import weka.core.Instances;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Searches a touche document collection.
 *
 */
public class ToucheSearcher
{
    /**
     * The fields of the typical touche topics.
     *
     */
    private static final class TOPIC_FIELDS
    {
        /**
         * The title of a topic.
         */
        public static final String TITLE = "title";

        /**
         * The description of a topic.
         */
        public static final String DESCRIPTION = "description";

        /**
         * The narrative of a topic.
         */
        public static final String NARRATIVE = "narrative";
    }


    /**
     * The identifier of the run.
     */
    private final String runID;

    /**
     * The run to be written.
     */
    private final PrintWriter run;

    /**
     * The index reader.
     */
    private final IndexReader reader;

    /**
     * The index searcher.
     */
    private final IndexSearcher searcher;

    /**
     * The topics to be searched.
     */
    private final QualityQuery[] topics;

    /**
     * The query parser.
     */
    private final QueryParser qp;

    /**
     * The maximum number of documents to retrieve.
     */
    private final int maxDocsRetrieved;

    /**
     * The number of documents to keep in the run.
     */
    private final int numDocsToKeep;

    /**
     * The number of words to use in ML phase.
     */
    private final int numTopWordsForML;

    /**
     * The total elapsed time.
     */
    private long elapsedTime = Long.MIN_VALUE;


    /**
     * Weighting factors used to score each words used in query expansion phase (TITLE query's field).
     */
    private static final double TITLE_TERM_SCORE  = 10.0;
    private static final double TITLE_OTHER_SCORE = 0.1 * TITLE_TERM_SCORE;
    private static final double TITLE_SYN_SCORE   = 0.25 * TITLE_TERM_SCORE;
    private static final double TITLE_ANT_SCORE   = 0.0 * TITLE_TERM_SCORE;

    /**
     * Weighting factors used to score each words used in query expansion phase (DESCRIPTION query's field).
     */
    private static final double DESCRIPTION_TERM_SCORE  = 1.0;
    private static final double DESCRIPTION_OTHER_SCORE = 0.6 * DESCRIPTION_TERM_SCORE;
    private static final double DESCRIPTION_SYN_SCORE   = 0.5 * DESCRIPTION_TERM_SCORE;
    private static final double DESCRIPTION_ANT_SCORE   = 0.2 * DESCRIPTION_TERM_SCORE;

    /**
     * Weighting factors used to score each words used in query expansion phase (NARRATIVE query's field).
     */
    private static final double NARRATIVE_TERM_SCORE  = 0.5;
    private static final double NARRATIVE_OTHER_SCORE = 0.8 * NARRATIVE_TERM_SCORE;
    private static final double NARRATIVE_SYN_SCORE   = 0.6 * NARRATIVE_TERM_SCORE;
    private static final double NARRATIVE_ANT_SCORE   = 0.2 * NARRATIVE_TERM_SCORE;

    /**
     * Weighting factor used to mix BM25 and ML scores.
     * alpha is the weight for BM25 and 1-alpha is the weight for ML.
     */
    private static final double ALPHA = 0.6;



    /**
     * Creates a new searcher.
     *
     * @param analyzer         the {@code Analyzer} to be used.
     * @param similarity       the {@code Similarity} to be used.
     * @param indexPath        the directory where containing the index to be searched.
     * @param topicsFile       the file containing the topics to search for.
     * @param expectedTopics   the total number of topics expected to be searched.
     * @param runID            the identifier of the run to be created.
     * @param runPath          the path where to store the run.
     * @param maxDocsRetrieved the maximum number of documents to be retrieved.
     * @param numDocsToKeep    the number of documents to keep in the run.
     * @param numTopWordsForML the number of most frequent words to consider during ML phase. See {@code linear_regression} for details
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public ToucheSearcher(final Analyzer analyzer, final Similarity similarity, final String indexPath,
                          final String topicsFile, final int expectedTopics, final String runID, final String runPath,
                          final int maxDocsRetrieved, final int numDocsToKeep, final int numTopWordsForML)
    {
        if (analyzer == null)
            throw new NullPointerException("Analyzer cannot be null.");

        if (similarity == null)
            throw new NullPointerException("Similarity cannot be null.");

        if (indexPath == null)
            throw new NullPointerException("Index path cannot be null.");

        if (indexPath.isEmpty())
            throw new IllegalArgumentException("Index path cannot be empty.");

        final Path indexDir = Paths.get(indexPath);
        if (!Files.isReadable(indexDir))
        {
            throw new IllegalArgumentException(String.format("Index directory %s cannot be read.",
                    indexDir.toAbsolutePath()));
        }

        if (!Files.isDirectory(indexDir))
        {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to search the index.",
                    indexDir.toAbsolutePath()));
        }

        try
        {
            reader = DirectoryReader.open(FSDirectory.open(indexDir));
        } catch (IOException e)
        {
            throw new IllegalArgumentException(String.format("Unable to create the index reader for directory %s: %s.",
                    indexDir.toAbsolutePath(), e.getMessage()), e);
        }

        searcher = new IndexSearcher(reader);
        searcher.setSimilarity(similarity);

        if (topicsFile == null)
            throw new NullPointerException("Topics file cannot be null.");

        if (topicsFile.isEmpty())
            throw new IllegalArgumentException("Topics file cannot be empty.");

        try
        {
            BufferedReader in = Files.newBufferedReader(Paths.get(topicsFile), StandardCharsets.UTF_8);

            topics = new XmlTopicsParser().readQueries(Paths.get(topicsFile).toString());

            in.close();
        } catch (Exception e)
        {
            throw new IllegalArgumentException(
                    String.format("Unable to process topic file %s: %s.", topicsFile, e.getMessage()), e);
        }

        if (expectedTopics <= 0)
        {
            throw new IllegalArgumentException(
                    "The expected number of topics to be searched cannot be less than or equal to zero.");
        }

        if (topics.length != expectedTopics)
        {
            System.out.printf("Expected to search for %s topics; %s topics found instead.",
                    expectedTopics, topics.length);
        }

        qp = new QueryParser(ParsedDocument.FIELDS.BODY, analyzer);

        if (runID == null)
            throw new NullPointerException("Run identifier cannot be null.");

        if (runID.isEmpty())
            throw new IllegalArgumentException("Run identifier cannot be empty.");

        this.runID = runID;


        if (runPath == null)
            throw new NullPointerException("Run path cannot be null.");

        if (runPath.isEmpty())
            throw new IllegalArgumentException("Run path cannot be empty.");

        final Path runDir = Paths.get(runPath);
        if (!Files.isWritable(runDir))
        {
            throw new IllegalArgumentException(
                    String.format("Run directory %s cannot be written.", runDir.toAbsolutePath()));
        }

        if (!Files.isDirectory(runDir))
        {
            throw new IllegalArgumentException(String.format("%s expected to be a directory where to write the run.",
                    runDir.toAbsolutePath()));
        }

        // Create the PrintWriter for the run to write to file.
        try
        {
            run = new PrintWriter(Files.newBufferedWriter(runDir.resolve(runID + ".txt"), StandardCharsets.UTF_8,
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE));
        } catch (IOException e)
        {
            throw new IllegalArgumentException(String.format("Unable to open run file: %s.", e.getMessage()), e);
        }


        if (maxDocsRetrieved <= 0)
            throw new IllegalArgumentException("The maximum number of documents to be retrieved must be positive.");

        if (numDocsToKeep <= 0)
            throw new IllegalArgumentException("The number of documents to keep in the run must be positive.");

        if (numTopWordsForML <= 0)
            throw new IllegalArgumentException("The number of words to use for ML phase must be positive.");

        this.maxDocsRetrieved = maxDocsRetrieved;

        this.numDocsToKeep = numDocsToKeep;

        this.numTopWordsForML = numTopWordsForML;


        final String datasetFilename = "touche/src/main/resources/CleanedDataset%d.csv".formatted(numTopWordsForML);

        if (!Files.exists(Paths.get(datasetFilename)))
        {
            try
            {
                final String inputFilename = "touche/src/main/resources/ArgumentDataset.csv";
                final String tempFilename = "touche/src/main/resources/ArgumentDatasetRaw.csv";

                CSVCleaner cleaner = new CSVCleaner();
                CSVCleaner.removeQuotationMarks(inputFilename, tempFilename);
                cleaner.cleanDataset(tempFilename, datasetFilename, numTopWordsForML);

                //Files.deleteIfExists(Paths.get(tempFilename));
            } catch (IOException e)
            {
                throw new RuntimeException("Error while loading the ML dataset.", e);
            }
        }
    }

    /**
     * Returns the total elapsed time.
     *
     * @return the total elapsed time.
     */
    public long getElapsedTime()
    {
        return elapsedTime;
    }


    private void buildQueryScoreMap(Map<String, Double> queryScoreMap, Analyzer analyzer,
                                    String data, Map<String, Tuple4<String, String[], String[], String[]>> dictionary,
                                    double termScore, double otherScore, double synScore, double antScore)
    {
        List<String> terms = Utils.parseStringWithAnalyzer(analyzer, data);
        for (String term : terms)
        {
            final double tScore = queryScoreMap.getOrDefault(term, 0.0) + termScore;
            queryScoreMap.put(term, tScore);

            Tuple4<String, String[], String[], String[]> tTuple = dictionary.get(term);
            if (tTuple == null)
                continue;


            final double kT = Double.min(1.0, 1.0 / (tTuple._2() == null ? 0.0 : tTuple._2().length)
                    + Double.MIN_VALUE);
            final double kS = Double.min(1.0, 1.0 / (tTuple._3() == null ? 0.0 : tTuple._3().length)
                    + Double.MIN_VALUE);
            final double kA = Double.min(1.0, 1.0 / (tTuple._4() == null ? 0.0 : tTuple._4().length)
                    + Double.MIN_VALUE);


            if (tTuple._2() != null)
            {
                for (String otherForm : tTuple._2())
                {
                    final double oScore = queryScoreMap.getOrDefault(otherForm, 0.0) + kT * otherScore;
                    queryScoreMap.put(otherForm, oScore);
                }
            }

            if (tTuple._3() != null)
            {
                for (String synonym : tTuple._3())
                {
                    final double sScore = queryScoreMap.getOrDefault(synonym, 0.0) + kS * synScore;
                    queryScoreMap.put(synonym, sScore);
                }
            }

            if (tTuple._4() != null)
            {
                for (String antonym : tTuple._4())
                {
                    final double aScore = queryScoreMap.getOrDefault(antonym, 0.0) + kA * antScore;
                    queryScoreMap.put(antonym, aScore);
                }
            }
        }
    }


    private Query buildQuery(Map<String, Double> queryScoreMap) throws Exception
    {
        StringBuilder sb = new StringBuilder();

        for (Map.Entry<String, Double> entry : queryScoreMap.entrySet())
            sb.append("body:").append(entry.getKey()).append("^").append(entry.getValue()).append(" OR ");

        return qp.parse(sb.substring(0, sb.length() - 4));
    }


    /**
     * /** Searches for the specified topics.
     *
     * @throws Exception if something goes wrong while searching.
     */
    public void search() throws Exception
    {
        //Create the tag of the run, reporting the alpha weight.
        final String runTag = runID + "_Mixed_" + ALPHA + "_" + numTopWordsForML;

        System.out.printf("%n#### Start searching ####%n");

        // the start time of the searching
        final long start = System.currentTimeMillis();

        final Set<String> idField = new HashSet<>();
        idField.add(ParsedDocument.FIELDS.ID);


        Query q;
        TopDocs docs;
        ScoreDoc[] sd;
        String docID;


        try
        {
            //Set up a linear regression to classify the combined quality of each document
            ToucheRegression lr = new ToucheRegression();

            // Load the training set for ML phase.
            final String datasetFilename = "touche/src/main/resources/CleanedDataset%d.csv".formatted(numTopWordsForML);
            lr.loadTrainingData(datasetFilename);

            //After this the regression model is ready to classify documents by their quality.
            lr.build();


            //Load the dictionary later used to do query expansion.
            Map<String, Tuple4<String, String[], String[], String[]>> dictionary = Utils.loadWordNetDictionary(
                    "dictionary.jsonl");


            for (QualityQuery t : topics)
            {
                System.out.printf("Searching for topic %s.%n", t.getQueryID());

                //******************** SEARCH PHASE ********************

                //Store the weight of each word used in the query.
                Map<String, Double> queryScoreMap = new HashMap<>();

                //Build the word-weight map using the TITLE field of the topic.
                String data = t.getValue(TOPIC_FIELDS.TITLE);
                if (data != null)
                {
                    buildQueryScoreMap(queryScoreMap, qp.getAnalyzer(), data, dictionary, TITLE_TERM_SCORE,
                            TITLE_OTHER_SCORE, TITLE_SYN_SCORE, TITLE_ANT_SCORE);
                }

                //Build the word-weight map using the DESCRIPTION field of the topic.
                data = t.getValue(TOPIC_FIELDS.DESCRIPTION);
                if (data != null && !data.isEmpty())
                {
                    buildQueryScoreMap(queryScoreMap, qp.getAnalyzer(), data, dictionary, DESCRIPTION_TERM_SCORE,
                            DESCRIPTION_OTHER_SCORE, DESCRIPTION_SYN_SCORE, DESCRIPTION_ANT_SCORE);
                }

                //Build the word-weight map using the NARRATIVE field of the topic.
                data = t.getValue(TOPIC_FIELDS.NARRATIVE);
                if (data != null && !data.isEmpty())
                {
                    buildQueryScoreMap(queryScoreMap, qp.getAnalyzer(), data, dictionary, NARRATIVE_TERM_SCORE,
                            NARRATIVE_OTHER_SCORE, NARRATIVE_SYN_SCORE, NARRATIVE_ANT_SCORE);
                }

                //Build the query using the word-weight map.
                q = buildQuery(queryScoreMap);


                //Perform the search of the most maxDocsRetrieved relevant documents for the topic.
                docs = searcher.search(q, maxDocsRetrieved);
                sd = docs.scoreDocs;


                //******************** ML PHASE ********************

                // START : machine learning contamination
                String path = "touche/src/main/resources/machine_learning_dataset/queryDataSet" +
                        t.getQueryID() + ".csv";

                boolean isQueryValid = true;

                ArrayList<String> docDataset = new ArrayList<>();
                ArrayList<String> docIDs = new ArrayList<>();
                for (int i = 0; i < maxDocsRetrieved; i++)
                {
                    // Get iterator for term vectors for each document
                    TermsEnum doc_terms = reader.getTermVector(sd[i].doc, "body").iterator();
                    StringBuilder sb = new StringBuilder();
                    // Append each token and build the document
                    for(BytesRef ref = doc_terms.next(); ref!=null; ref=doc_terms.next()){
                        sb.append(ref.utf8ToString()).append(" ");
                    }
                    String cleaned_document = sb.toString();

                    if(!cleaned_document.isEmpty()) {
                        docDataset.add(cleaned_document);
                        docIDs.add(reader.document(sd[i].doc, idField).get(ParsedDocument.FIELDS.ID));
                    }
                }

                //START : encoded dataset creation
                Encoder encoder = new Encoder();
                //Create a csv file containing the samples
                encoder.featureExtractor(docDataset, numTopWordsForML + 1, path);    // Extract feature based on most frequent terms for each query
                //END   : encoded dataset creation

                //Transform the samples on csv file in a weka readable object
                Map<String, Double> combinedQualities = new HashMap<>(); // (docID, quality of this document)
                try {
                    Instances samples = lr.fromCsvToInstances(path);
                    for (int i = 0; i < samples.size(); i++) { //For each sample, classify it
                        //Link each DocID with his predicted quality score, fill the map
                        combinedQualities.put(docIDs.get(i), lr.classify(samples.instance(i)));
                    }
                }
                catch(Exception e){
                    //Something went wrong with the file reading/writing
                    //Just skip this query
                    isQueryValid = false;
                    e.printStackTrace();
                }
                //Now we have the map filled. Need to sort it in descending order by value
                Map<String, Double> sortedCombinedQualities = combinedQualities.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                        .collect(Collectors.toMap(
                                Map.Entry::getKey,
                                Map.Entry::getValue,
                                (x,y)-> {throw new AssertionError();},
                                LinkedHashMap::new
                        ));
                // END : machine learning contamination

                //******************** OUTPUT OF THE RUN PHASE ********************

                // Check if the ML phase has been performed without issues.
                if(isQueryValid)
                {
                    //Find maximum and minimum value of both scores.
                    double minScoreBM25 = Double.MAX_VALUE;
                    double maxScoreBM25 = -Double.MAX_VALUE;
                    double minScoreML = Double.MAX_VALUE;
                    double maxScoreML = -Double.MAX_VALUE;


                    for (ScoreDoc scoreDoc : sd)
                    {
                        docID = reader.document(scoreDoc.doc, idField).get(ParsedDocument.FIELDS.ID);

                        final double scoreBM25 = scoreDoc.score;
                        final double scoreML = sortedCombinedQualities.get(docID);

                        if (scoreBM25 > maxScoreBM25)
                            maxScoreBM25 = scoreBM25;

                        if (scoreBM25 < minScoreBM25)
                            minScoreBM25 = scoreBM25;

                        if (scoreML > maxScoreML)
                            maxScoreML = scoreML;

                        if (scoreML < minScoreML)
                            minScoreML = scoreML;
                    }


                    //Compute the combined score of each document.
                    Map<String, Double> mixedScoreMap = new HashMap<>();

                    for (ScoreDoc scoreDoc : sd)
                    {
                        docID = reader.document(scoreDoc.doc, idField).get(ParsedDocument.FIELDS.ID);

                        final double scoreBM25 = scoreDoc.score;
                        final double scoreML = sortedCombinedQualities.get(docID);

                        //Normalize BM25 and ML scores in [0.0, 1.0] range.
                        final double normScoreBM25 = (scoreBM25 - minScoreBM25) / (maxScoreBM25 - minScoreBM25);
                        final double normScoreML = (scoreML - minScoreML) / (maxScoreML - minScoreML);

                        //Compute the mixed score.
                        final double mixedScore = ALPHA * normScoreBM25 + (1.0 - ALPHA) * normScoreML;
                        mixedScoreMap.put(docID, mixedScore);
                    }

                    //Sort the documents by their score.
                    List<Map.Entry<String, Double>> mixedScoreSorted = new ArrayList<>(mixedScoreMap.entrySet());
                    mixedScoreSorted.sort(Map.Entry.comparingByValue());


                    //Write the run to file.
                    final int num = Integer.min(sd.length, numDocsToKeep);
                    for (int i = 0; i < num; i++)
                    {
                        final int index = mixedScoreSorted.size() - 1 - i;

                        docID = mixedScoreSorted.get(index).getKey();
                        final double mixedScore = mixedScoreSorted.get(index).getValue();

                        run.printf(Locale.ENGLISH, "%s\tQ0\t%s\t%d\t%.6f\t%s%n", t.getQueryID(), docID,
                                i, mixedScore, runTag);
                    }

                    run.flush();
                }
                else
                    throw new RuntimeException("An error has occurred during ML phase.");
            }
        } catch (Exception e)
        {
            throw new RuntimeException("Exception while search()", e);
        } finally
        {
            run.close();
            reader.close();
        }

        elapsedTime = System.currentTimeMillis() - start;

        System.out.printf("%d topic(s) searched in %d seconds.\n", topics.length, getElapsedTime() / 1000);

        System.out.printf("#### Searching complete ####%n");
    }
}