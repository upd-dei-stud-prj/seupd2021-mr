package exec;

import analyze.ToucheAnalyzer;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.similarities.BM25Similarity;
import search.ToucheSearcher;

/**
 * Class used to search every query and build the run files.
 */
public class Searching
{
    public static void main(String[] args) throws Exception
    {
        //File which contains the topics to search.
        final String topics = "touche/topics/topics-2020.xml";

        //Folder where the Lucene index is stored.
        final String indexPath = "touche/experiment/index";

        //Folder where to store the run file.
        final String runPath = "touche/experiment";

        //Name of the run;
        final String runID = "run2020";

        //Number of documents to retrieve during search phase.
        final int maxDocsRetrieved = 1000;

        //Number of documents, sorted by score descending, to put in the run file.
        final int numDocsToKeep = 1000;

        //Number of words to use for ML phase.
        final int numTopWordsForML = 250;


        final Analyzer a = new ToucheAnalyzer();

        ToucheSearcher s = new ToucheSearcher(a, new BM25Similarity(), indexPath, topics, 49, runID,
                runPath, maxDocsRetrieved, numDocsToKeep, numTopWordsForML);

        s.search();
    }
}