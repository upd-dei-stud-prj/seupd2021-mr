package exec;

import analyze.ToucheAnalyzer;
import index.ToucheIndexer;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.similarities.BM25Similarity;

/**
 * Class used to build the index.
 */
public class Indexing
{

    public static void main(String[] args) throws Exception
    {
        //Size of internal buffer in MB used by Lucene during indexing.
        final int ramBuffer = 256;

        //Folder where the corpus files are stored.
        final String docsPath = "touche/corpus";

        //Folder where to store the index.
        final String indexPath = "touche/experiment/index";

        //Extension of the corpus files.
        final String extension = "json";

        //Number of expected documents present in the corpus.
        final int expectedDocs = 387740;

        //Charset used to encode the documents in the corpus.
        final String charsetName = "ISO-8859-1";


        final Analyzer a = new ToucheAnalyzer();

        ToucheIndexer i = new ToucheIndexer(a, new BM25Similarity(), ramBuffer, indexPath, docsPath, extension,
                charsetName, expectedDocs);

        i.index();
    }
}