package exec;

import analyze.WordnetDictionaryEntry;
import com.google.gson.Gson;
import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import scala.Tuple4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Miscellaneous collection of various methods.
 */
public final class Utils
{
    /**
     * Load the requested stop list from file.
     *
     * @param filename The name of the file containing the stop list.
     * @return The list of tokens of the stop list.
     */
    public static CharArraySet loadStopList(final String filename)
    {
        //Check if filename is null or empty.
        if (filename == null)
            throw new NullPointerException("Stop list file name cannot be null.");

        if (filename.isEmpty())
            throw new IllegalArgumentException("Stop list file name cannot be empty.");

        final Path path = Paths.get("touche/src/main/resources/" + filename);

        try
        {
            final List<String> data = Files.readAllLines(path, StandardCharsets.UTF_8);

            return StopFilter.makeStopSet(data);
        } catch (IOException e)
        {
            throw new RuntimeException("Unable to load the stoplist.", e);
        }
    }


    /**
     * Load the requested Wordnet dictionary.
     *
     * @param filename The name of the file containing the dictionary.
     * @return The requested dictionary.
     */
    public static Map<String, Tuple4<String, String[], String[], String[]>> loadWordNetDictionary(
            String filename)
    {
        final Path path = Paths.get("touche/src/main/resources/" + filename);

        try
        {
            Map<String, Tuple4<String, String[], String[], String[]>> result = new HashMap<>();

            try (BufferedReader reader = Files.newBufferedReader(path))
            {
                Gson gson = new Gson();

                String line;
                while ((line = reader.readLine()) != null)
                {
                    WordnetDictionaryEntry obj = gson.fromJson(line, WordnetDictionaryEntry.class);

                    result.put(obj.word, new Tuple4<>(obj.pos, obj.otherForms, obj.synonyms, obj.antonyms));
                }
            }

            return result;
        }
        catch (Throwable th)
        {
            throw new RuntimeException("An error occurred while loading the Wordnet dictionary.", th);
        }
    }



    /**
     * Parse the given string with the given analyzer and returns the list of tokens produced.
     *
     * @param analyzer The analyzer to use to parse the data.
     * @param data The string to analyze.
     * @return The list of tokens produced.
     */
    public static List<String> parseStringWithAnalyzer(Analyzer analyzer, String data)
    {
        try
        {
            List<String> result = new ArrayList<>();
            TokenStream stream  = analyzer.tokenStream("field", new StringReader(data));

            stream.reset();
            while (stream.incrementToken())
            {
                CharTermAttribute cAttr = stream.getAttribute(CharTermAttribute.class);
                OffsetAttribute oAttr = stream.getAttribute(OffsetAttribute.class);

                String term = cAttr.toString();
                final int start = oAttr.startOffset();
                final int end = oAttr.endOffset();


                result.add(term);
                //result.add(String.format("\"%s\"\t%d\t%d", cAttr, start, end));
            }
            stream.end();
            stream.close();

            return result;
        }
        catch (Throwable th)
        {
            throw new RuntimeException("An error while analyzing the string has occurred.", th);
        }
    }
}