package exec;

import parse.ParsedDocument;
import parse.ToucheDocument;
import parse.ToucheParser;

import java.io.FileReader;

/**
 * Class used to test the parsing part and get a look of some documents for a chosen corpus file.
 */
public class Parsing
{
    public static void main(String[] args) throws Exception
    {
        // Change here the corpus file to scan
        final String path = "touche/corpus/debateorg.json";
        //final String path = "touche/corpus/debatepedia.json";
        //final String path = "touche/corpus/debatewise.json";
        //final String path = "touche/corpus/idebate.json";
        //final String path = "touche/corpus/parliamentary.json";

        ToucheParser parser = new ToucheParser(new FileReader(path));

        long time = System.currentTimeMillis();

        int numDocs = 0;

        // Change here the number of document to visualize
        int numDocsToRetrieve = 60;

        // Print the documents
        for (ToucheDocument doc : parser)
        {

            ParsedDocument parsedDoc = doc.toParsedDocument();
            System.out.println(parsedDoc);
            numDocs++;

            if(numDocs == numDocsToRetrieve)
                break;
        }

        time = System.currentTimeMillis() - time;

        parser.close();


        System.out.println("Took " + time + " ms.\nRetrieved " + numDocs + " documents.");
    }
}
