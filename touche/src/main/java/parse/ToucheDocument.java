package parse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * Class that encapsulates only the common relevant fields between all the corpus document type.
 * It represent the first transformation from a json file.
 */
public final class ToucheDocument
{
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private static final String separator = " ";

    private static final class Premise
    {
        public String text;
    }


    // Introduction fields
    public String id;
    public String conclusion;
    public Premise[] premises;


    /**
     * Transform this {@code ToucheDocument} in a {@code ParsedDocument} later
     * used in the indexing phase.
     *
     * @return {@code ParsedDocument} made of
     * ID:   this ID field.
     * BODY: concatenation of all and only the relevant fields of the document.
     *
     * @throws Exception If the retrieved ID field is null.
     */
    public ParsedDocument toParsedDocument() throws Exception
    {
        if (id == null)
            throw new Exception("Document ID is null");


        // Build the body. Add a newline at the start to avoid crashing if both
        // conclusion and premises.text are null.
        StringBuilder body = new StringBuilder();
        body.append(separator);

        // Add the relevant fields to the BODY.
        if (conclusion != null)
            body.append(conclusion).append(separator);

        if (premises != null)
        {
            for (Premise premise : premises)
            {
                if (premise.text != null)
                    body.append(premise.text).append(separator);
            }
        }


        return new ParsedDocument(this.id, body.toString());
    }


    /**
     * @return A JSON structured string representation of this document,
     * containing only the relevant fields.
     */
    public String toString()
    {
        return GSON.toJson(this);
    }
}