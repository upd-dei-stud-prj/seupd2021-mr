package parse;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;


/**
 * Class used to deserialize args.me corpus - all variants - in streaming fashion.
 * It uses Google's Gson library under the hood.
 */
public class ToucheParser implements Iterator<ToucheDocument>, Iterable<ToucheDocument>
{
    private final JsonReader reader;
    private final Gson gson;

    /**
     * Create a new {@code ToucheParser} for deserializing args.me corpus.
     *
     * @param reader Reader to a JSON file containing the data.
     * @throws Exception If the creation of the parser fails.
     */
    public ToucheParser(final Reader reader) throws Exception
    {
        this.reader = new JsonReader(reader);
        gson = new Gson();

        try
        {
            // Skips the {"arguments":[ initial junk at the start of the corpus.
            this.reader.beginObject();
            this.reader.nextName();
            this.reader.beginArray();
        } catch (IOException e)
        {
            throw new Exception("Error initializing the parser.");
        }
    }


    /**
     * Closes the internal reader used by the parser.
     *
     * @throws Exception If the reader close() fails.
     */
    public void close() throws Exception
    {
        reader.close();
    }


    /**
     * Returns an iterator for the documents of the corpus.
     *
     * @return An iterator for the documents of the corpus.
     */
    @Override
    public Iterator<ToucheDocument> iterator()
    {
        return this;
    }


    /**
     * Check if there are documents yet to parse.
     *
     * @return {@code true} if there are documents yet to parse, {@code false} otherwise.
     */
    @Override
    public boolean hasNext()
    {
        try
        {
            // Must take care of the ]} junk at the end of the corpus.
            return (reader.hasNext()) && (!reader.peek().equals(JsonToken.END_ARRAY));
        } catch (IOException e)
        {
            return false;
        }
    }


    /**
     * Retrieve the next document from the reader.
     *
     * @return The next document from the reader, if available, otherwise {@code null}.
     */
    @Override
    public ToucheDocument next()
    {
        if (!hasNext())
            return null;

        return gson.fromJson(reader, ToucheDocument.class);
    }
}