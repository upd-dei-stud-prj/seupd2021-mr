package analyze;

import exec.Utils;
import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.en.EnglishPossessiveFilter;
import org.apache.lucene.analysis.pattern.PatternReplaceCharFilter;

import java.io.Reader;
import java.util.regex.Pattern;

/**
 * Custom analyzer for processing touche documents collection
 */
public class ToucheAnalyzer extends Analyzer
{
    /**
     * Try to delete all links present in the documents.
     */
    public static final Pattern LINKS_PATTERN = Pattern.compile(
            "(?:file|ftp|http|https|uri|url)://\\S+|www\\.\\S+", Pattern.DOTALL);

    /**
     * Used to convert all invalid characters in the # character.
     */
    private static final Pattern JUNK_CHARS_PATTERN = Pattern.compile("[^A-za-z\\s,.;:\\-_!?'\"]");

    /**
     * Used to convert all punctuation characters in whitespaces.
     */
    private static final Pattern PUNCT_CHARS_PATTERN = Pattern.compile("[\\s,.;:\\-_!?\"]");

    /**
     * Discard all tokens that don't contain only letters.
     */
    public static final Pattern DISCARD_PATTERN = Pattern.compile("[^a-z]", Pattern.DOTALL);


    /**
     * Creates a new instance of the analyzer.
     */
    public ToucheAnalyzer()
    {
        super();
    }


    @Override
    protected TokenStreamComponents createComponents(String s)
    {
        final Tokenizer source = new WhitespaceTokenizer();

        TokenStream tokens = new EnglishPossessiveFilter(source);
        tokens = new LowerCaseFilter(tokens);
        tokens = new StopFilter(tokens, Utils.loadStopList("stoplist.txt"));
        tokens = new DiscardTokenFilter(tokens, DISCARD_PATTERN);

        //Actually detrimental to the #relevant doc. retrieved.
        //tokens = new PorterStemFilter(tokens);

        return new TokenStreamComponents(source, tokens);
    }

    @Override
    protected Reader initReader(String fieldName, Reader reader)
    {
                //Replace all punctuation characters with whitespaces.
        return new PatternReplaceCharFilter(PUNCT_CHARS_PATTERN, " ",
                //Replace all characters that are never included in text with '#',
                //in order to flag that token as invalid and later discard it.
                new PatternReplaceCharFilter(JUNK_CHARS_PATTERN, "#",
                //Try to discard all links in the documents, replacing them with '#'.
                new PatternReplaceCharFilter(LINKS_PATTERN, "#", reader)));
    }
}