package analyze;

import org.apache.lucene.analysis.FilteringTokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Token filter that discards all tokens that have a match with the discard pattern.
 */
public class DiscardTokenFilter extends FilteringTokenFilter
{
    private final Pattern discard_pattern;
    private final CharTermAttribute termAttribute;


    public DiscardTokenFilter(TokenStream in, Pattern pattern)
    {
        super(in);
        termAttribute = addAttribute(CharTermAttribute.class);

        discard_pattern = pattern;
    }

    @Override
    protected boolean accept() throws IOException
    {
        final String token = termAttribute.toString();

        return !discard_pattern.matcher(token).find();
    }
}