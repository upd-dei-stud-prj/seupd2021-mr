package analyze;

public final class WordnetDictionaryEntry
{
    public String word;
    public String pos;
    public String[] otherForms;
    public String[] synonyms;
    public String[] antonyms;
}