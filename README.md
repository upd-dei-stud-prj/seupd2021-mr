# Search Engines Project 20/21

Touché Task 1: Argument Retrieval for Controversial Questions

### Corpus
All the corpus files should be inside the directory `touche/corpus`.
We used the args.me corpus (version 2020-04-01) available at https://zenodo.org/record/3734893#.YIxOLbUzaUk

### Index
The index is built inside the directory `touche/experiment/index`.

### Runs
We provided some runs for the topics-2020 and topics-2021 in the directories `touche/runs/runs2020` and `touche/runs/runs2021`.
When searching for each query, the runs file are stored inside the directory `touche/experiment`.

### Report
The final report and the slides for the presentation are inside the directory `report`.

### Topics
The XML files containing the 2020 and 2021 topics are inside the directory `touche/topics`.

### Statistical analysis
The code and data related to the statistical analysis are inside the directory `statistical_analysis`.